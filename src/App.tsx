import React, { useState } from "react";
import "./styles/App.scss";
import BookSearch from "./book-search/BookSearch";
import WishList from "./wish-list/Wishlist";
import { BookContextProvider } from "./book-context/BookContext";

function App() {
  return (
    <div>
      <header className="header">
        <div className="header--content">
          <h1>My Good Reads</h1>
        </div>
      </header>
      <main>
        <BookContextProvider>
          <div className="wrapper">
            <div className="search-container">
              <BookSearch />
            </div>
            <div className="reading-list-container">
              <WishList />
            </div>
          </div>
        </BookContextProvider>
      </main>
    </div>
  );
}

export default App;
