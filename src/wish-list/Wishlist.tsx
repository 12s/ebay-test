import "../styles/WishList.scss";
import React from "react";
import { useBooks } from "../book-context/BookContext";

const WishList = () => {
  const { wishList, setWishList } = useBooks();

  return (
    <>
      <div className="search--container">
        <h2>My Reading List</h2>

        {wishList &&
          [...wishList].map((book: any, i) => {
            return (
              <div key={i} className="wishlist-wrapper">
                <div>
                  <img
                    alt="logo"
                    src={`${
                      book.volumeInfo.imageLinks &&
                      book.volumeInfo.imageLinks.smallThumbnail
                    }`}
                  />
                </div>
                <div className="wishlist-info">
                  <div>{book.volumeInfo.publisher}</div>
                  <div>{book.volumeInfo.publishedDate}</div>
                </div>
                <div className="button-wrapper">
                  <button
                    className="button-class"
                    onClick={() => {
                      const netSet = new Set(wishList);
                      netSet.delete(book);
                      setWishList(netSet);
                    }}
                  >
                    Remove from cart
                  </button>
                </div>
              </div>
            );
          })}
      </div>
    </>
  );
};

export default WishList;
