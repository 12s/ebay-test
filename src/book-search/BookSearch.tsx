import React, { useEffect, useState } from "react";
import "../styles/BookSearch.scss";
import { getBooksByType } from "./book-search.service";
import { useBooks } from "../book-context/BookContext";

const BookSearch = () => {
  const [bookTypeToSearch, updateBookTypeToSearch] = useState("");
  const [timerID, setTimerId] = useState();

  const {
    allAvailableBooks,
    setBookResults,
    wishList,
    setWishList,
  } = useBooks();

  useEffect(() => {
    async function getAllBooks() {
      if (timerID) {
        clearTimeout(timerID);
      }
      let timerId: any = await setTimeout(async () => {
        const allBooks = await getBooksByType(bookTypeToSearch);
        setBookResults(allBooks);
      }, 700);
      setTimerId(timerId);
    }

    getAllBooks();
  }, [bookTypeToSearch]);

  return (
    <>
      <div className="book--container">
        <div className="search-params">
          <div>
            <form>
              <input
                className="full-width"
                autoFocus
                name="gsearch"
                type="search"
                placeholder="Search for books to add to your reading list and press Enter"
                onChange={(e) => {
                  updateBookTypeToSearch(e.target.value);
                }}
              />
            </form>
            {allAvailableBooks.length === 0 && timerID < 3 && (
              <div className="empty">
                <p>
                  Try searching for a topic, for example
                  <a
                    onClick={() => {
                      updateBookTypeToSearch("Javascript");
                    }}
                  >
                    {" "}
                    "Javascript"
                  </a>
                </p>
              </div>
            )}
            {allAvailableBooks.length === 0 && timerID > 3 && (
              <div className="empty">
                <p>Loading...</p>
              </div>
            )}
          </div>
        </div>
      </div>
      {allAvailableBooks &&
        allAvailableBooks.items &&
        allAvailableBooks.items.map((book: any, j: number) => {
          return (
            <div key={j} className="row-wrapper">
              <img
                alt="logo"
                src={`${
                  book.volumeInfo.imageLinks &&
                  book.volumeInfo.imageLinks.smallThumbnail
                }`}
              />
              <div className="description-wrapper">
                <div>{book.volumeInfo.title}</div>
                <div>
                  {book.volumeInfo.authors &&
                    book.volumeInfo.authors.map((author: string, i: number) => {
                      return (
                        <span key={i} className="authorName">
                          {author}
                        </span>
                      );
                    })}
                </div>
                <div>{book.volumeInfo.publisher}</div>
                <div>{book.volumeInfo.publishedDate}</div>
                <div>{book.volumeInfo.description}</div>
                <button
                  onClick={() => {
                    const netSet = new Set(wishList);
                    netSet.add(book);
                    setWishList(netSet);
                  }}
                >
                  Add to Cart
                </button>
              </div>
            </div>
          );
        })}
    </>
  );
};

export default BookSearch;
