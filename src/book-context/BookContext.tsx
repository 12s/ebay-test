import React, { useState, useContext } from "react";

// interface IBookInfo {
//   volumeInfo: {
//     title: String;
//     imageLinks: { smallThumbnail: String };
//     authors: Array<String>;
//     publisher: String;
//     publishedDate: String;
//     description: String;
//   };
//   id: String;
// }

// interface IBooks {
//   items: Array<IBookInfo>;
//   kind: String;
//   totalItems: Number;
//   wishList: Set<{}>;
// }

// interface Ichildren {
//   children: any;
// }

const BookContext = React.createContext({} as any);

export function useBooks() {
  return useContext(BookContext);
}

export function BookContextProvider({ children }: any) {
  const [allAvailableBooks, setBookResults] = useState<any | undefined>([]);
  const [wishList, setWishList] = useState(new Set());

  return (
    <BookContext.Provider
      value={{ allAvailableBooks, setBookResults, wishList, setWishList }}
    >
      {children}
    </BookContext.Provider>
  );
}
